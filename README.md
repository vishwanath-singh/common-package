# common-package

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/common-package.svg)](https://www.npmjs.com/package/common-package) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save common-package
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'common-package'
import 'common-package/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
